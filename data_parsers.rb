require 'json'
require 'stringex'

def parse_base_words(objectives_data)
  p "Parsing objectives -> base words...\n"

  objectives_data.map {|objective_data|
    p "Parsing objective #{objective_data["text"]}"
    
    {
      "_id" => "en-vi_" + objective_data["text"].downcase,
      "text" => objective_data["text"],
      "is_objective" => true,
      "lang" => "en",
      "image_files" => (1..3).map {|i| objective_data["image_#{i}"]},
      "sound" => objective_data["sound"]
    }
  }
end

def parse_base_skills(skills_data, objectives_data, objectives_lessons_data)
  p "Parsing skills -> base skills...\n"

  skills_data.map {|skill_data|
    p "Parsing skill #{skill_data["name"]}"

    lessons = (skill_data["lessons"].to_i-1).times.map {|i|
      objective_ids = objectives_lessons_data.select {|relation|
        relation["skill_rid"] == skill_data["rid"] && relation["lesson_number"] == (i+1).to_s
      }.map {|objective| objective["objective_id"]}

      {
        "lesson_number" => i+1,
        "base_word_ids" => objectives_data.select {|objective|
          objective_ids.include?(objective["id"])
        }.map {|objective| "en-vi_" + objective["text"].downcase}
      }
    }

    {
      "_id" => "en-vi_" + skill_data["title"].to_url.gsub('-', '_').downcase,
      "name" => skill_data["name"],
      "base_course_id" => "en-vi",
      "base_word_ids" => lessons.map {|lesson| lesson["base_word_ids"]}.flatten.uniq,
      "lessons" => lessons
    }
  }
end

def parse_sentences(sentences_data)
  p "Parsing sentences...\n"

  sentences_data.map {|sentence_data|
    next if sentence_data['text'].blank?

    p "Parsing sentence #{sentence_data["text"]}"

    sentence = {
      "compact_translation" => sentence_data["text"],
      "lang" => sentence_data["language"],
      "group" => sentence_data["level"].to_i
    }

    if sentence["lang"] == "en"
      sentence.merge!({
        "normal_audio_file" => sentence_data["normal_sound"],
        "slow_audio_file" => sentence_data["slow_sound"]
      })
    end

    sentence
  }
end

def parse_sentences_by_lessons(lessons_sentences_data, base_skills)
  p "Parsing sentences-lessons relationships...\n"

  sentence_texts = lessons_sentences_data.map {|relation| relation["sentence_text"]}.uniq

  sentence_texts.map {|text|
    p "Parsing sentence #{text}"

    relations = lessons_sentences_data.select {|relation| relation["sentence_text"] == text}

    {
      "compact_translation" => text,
      "base_skill_ids" => relations.map {|relation|
        base_skill = base_skills.find {|bs| bs["name"] == relation["skill_name"]}
        base_skill["_id"]
      }.uniq,
      "base_lesson_ids" => relations.map {|relation|
        base_skill = base_skills.find {|bs| bs["name"] == relation["skill_name"]}
        base_skill["_id"] + "|" + relation["lesson_number"]
      }.uniq
    }
  }
end

def parse_sentences_by_objectives(sentences_objectives_data)
  p "Parsing sentences-objectives relationships...\n"

  sentence_texts = sentences_objectives_data.map {|sentence_data|
    sentence_data["sentence_text"]
  }.uniq

  sentence_texts.map {|text|
    p "Parsing sentence #{text}"

    {
      "compact_translation" => text,
      "base_word_ids" => sentences_objectives_data.select {|relation|
        relation["sentence_text"] == text
      }.map {|relation|
        "en-vi_" + relation["objective_text"].downcase
      }.uniq
    }
  }
end

def parse_sentences_by_sentences(sentences_sentences_data)
  p "Parsing sentences-sentences relationships...\n"

  valid_couples = sentences_sentences_data.select {|couple|
    (couple["ll_text"] || "") != "" && (couple["ml_text"] || "") != ""
  }
    
  valid_couples.map {|couple|
    p "Parsing sentences #{couple["ll_text"]} - #{couple["ml_text"]}\n"

    {
      "en_text" => couple["ll_text"],
      "vi_text" => couple["ml_text"]
    }
  }
end

def parse_data
  prefix = "memo_"
  names = Dir["#{prefix}*.json"].map {|f| f.gsub(/(#{prefix}|\.json)/, '')}
  
  raw_data = {}

  names.map {|name|
    filename = "#{prefix}#{name}.json"
    raw_data[name] = JSON.parse(File.read(filename))
    true
  }.uniq

  parsed_data = {}

  parsed_data["base_words"] = parse_base_words(raw_data["objective"]); true
  parsed_data["base_skills"] = parse_base_skills(raw_data['skill'], raw_data['objective'], raw_data['objective_lesson']); true

  parsed_data["sentences"] = parse_sentences(raw_data["sentence"])
  parsed_data["sentences_link_skills_lessons"] = parse_sentences_by_lessons(raw_data["lesson_sentence"], parsed_data["base_skills"]); true
  parsed_data["sentences_link_base_words"] = parse_sentences_by_objectives(raw_data["sentence_objective"]); true
  parsed_data["sentences_link_sentences"] = parse_sentences_by_sentences(raw_data["sentence_sentence"]); true

  parsed_data.map {|k, v|
    File.open("output_#{k}.json", "w") {|f| f.write(v.to_json)}
    true
  }.uniq
end