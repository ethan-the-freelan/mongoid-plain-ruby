Usage guide

- Enter `irb`
- Load source files:

    `load 'console.rb'`
    
- To seeds data, run:

    `seeds_data`