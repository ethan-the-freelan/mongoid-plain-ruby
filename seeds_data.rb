require 'csv'

def seed_courses
  BaseCourse.collection.drop
  BaseCourse.create({
    "_id" => "en-vi",
    "name" => "Tiếng Anh",
    "primary_lang" => "vi",
    "learning_lang" => "en",
    "checkpoints" => [
      {
        "row" => 4,
        "highest_skill_id" => "en-vi_dai_tu_khach_quan",
        "accepted_test_times" => 3
      },
      {
        "row" => 9,
        "highest_skill_id" => "en-vi_gia_dinh",
        "accepted_test_times" => 3
      },
      {
        "row" => 17,
        "highest_skill_id" => "en-vi_giao_duc",
        "accepted_test_times" => 3
      },
      {
        "row" => 25,
        "highest_skill_id" => "en-vi_thien_nhien",
        "accepted_test_times" => 3
      }
    ]
  })
end

def seed_levels
  BaseLevel.collection.drop

  [
    {"_id"=>1, "title"=>"", "exp"=>0},
    {"_id"=>2, "title"=>"", "exp"=>60},
    {"_id"=>3, "title"=>"", "exp"=>120},
    {"_id"=>4, "title"=>"", "exp"=>200},
    {"_id"=>5, "title"=>"", "exp"=>300},
    {"_id"=>6, "title"=>"", "exp"=>450},
    {"_id"=>7, "title"=>"", "exp"=>750},
    {"_id"=>8, "title"=>"", "exp"=>1125},
    {"_id"=>9, "title"=>"", "exp"=>1650},
    {"_id"=>10, "title"=>"", "exp"=>2250},
    {"_id"=>11, "title"=>"", "exp"=>3000},
    {"_id"=>12, "title"=>"", "exp"=>3900},
    {"_id"=>13, "title"=>"", "exp"=>4900},
    {"_id"=>14, "title"=>"", "exp"=>6000},
    {"_id"=>15, "title"=>"", "exp"=>7500},
    {"_id"=>16, "title"=>"", "exp"=>9000},
    {"_id"=>17, "title"=>"", "exp"=>10500},
    {"_id"=>18, "title"=>"", "exp"=>12000},
    {"_id"=>19, "title"=>"", "exp"=>13500},
    {"_id"=>20, "title"=>"", "exp"=>15000},
    {"_id"=>21, "title"=>"", "exp"=>17000},
    {"_id"=>22, "title"=>"", "exp"=>19000},
    {"_id"=>23, "title"=>"", "exp"=>22500},
    {"_id"=>24, "title"=>"", "exp"=>26000},
    {"_id"=>25, "title"=>"", "exp"=>30000}
  ].each {|level|
    BaseLevel.create(level)
  }
end

def seeds_items
  items = [
    {
      "_id" => "health_potion",
      "section" => "doping",
      "localized" => {
        "section" => {
          "en" => "DOPING",
          "vi" => "THUỐC KÍCH THÍCH"
        },
        "name" => {
          "en" => "Health Potion",
          "vi" => "Hồi Máu"
        },
        "info" => {
          "en" => "Health Potion allows you to regain one Health lost during a lesson.",
          "vi" => "Hồi Máu cho phép bạn có thêm một Máu trong trường hợp bạn đã mất hết mạng trong một bài học."
        }
      },
      "price" => 4,
      "consumable" => true,
      "max_owned_amount" => 1,
      "platforms" => ["ios", "android", "web"],
      "enabled" => true
    },
    {
      "_id" => "combo_protect",
      "section" => "doping",
      "localized" => {
        "section" => {
          "en" => "DOPING",
          "vi" => "THUỐC KÍCH THÍCH"
        },
        "name" => {
          "en" => "Combo Protect",
          "vi" => "Bảo vệ Combo"
        },
        "info" => {
          "en" => "Combo Protect allows your Combo to remain in place for one full day of inactivity.",
          "vi" => "Bảo vệ Combo cho phép bạn giữ nguyên Combo trong một ngày không hoạt động."
        }
      },
      "price" => 10,
      "consumable" => true,
      "max_owned_amount" => 1,
      "platforms" => ["web"],
      "enabled" => false
    },
    {
      "_id" => "lucky_star",
      "section" => "doping",
      "localized" => {
        "section" => {
          "en" => "DOPING",
          "vi" => "THUỐC KÍCH THÍCH"
        },
        "name" => {
          "en" => "Lucky Star",
          "vi" => "Ngôi sao May mắn"
        },
        "info" => {
          "en" => "Attempt to double your 5 MemoCoin wager by maintaining a seven-day Combo.",
          "vi" => "Nhận được gấp đôi số MemoCoin từ 5 MemoCoin mà bạn bỏ ra nếu bạn giữ được 7 ngày Combo."
        }
      },
      "price" => 5,
      "consumable" => true,
      "max_owned_amount" => 1,
      "platforms" => ["web"],
      "enabled" => false
    },
    {
      "_id" => "countdown_practice",
      "section" => "practice",
      "localized" => {
        "section" => {
          "en" => "PRACTICE",
          "vi" => "THỰC HÀNH"
        },
        "name" => {
          "en" => "Countdown Practice",
          "vi" => "Luyện tập Tính giờ"
        },
        "info" => {
          "en" => "See how well you do practicing your skills against the clock in Countdown Practice.",
          "vi" => "Thử trả lời tất cả câu hỏi trước khi hết thời gian, đồng thời rèn luyện tính phản xạ cũng như tất cả kiến thức bạn học được."
        }
      },
      "price" => 10,
      "consumable" => false,
      "max_owned_amount" => 1,
      "platforms" => ["web"],
      "enabled" => false
    },
    {
      "_id" => "progress_quiz",
      "section" => "practice",
      "localized" => {
        "section" => {
          "en" => "PRACTICE",
          "vi" => "THỰC HÀNH"
        },
        "name" => {
          "en" => "Progress Quiz",
          "vi" => "Bài kiểm tra"
        },
        "info" => {
          "en" => "Take an extended quiz to measure your language learning progress.",
          "vi" => "Thực hiện một bài kiểm tra mở rộng để đo lường tiến bộ học tập ngôn ngữ của bạn."
        }
      },
      "price" => 25,
      "consumable" => false,
      "max_owned_amount" => 1,
      "platforms" => ["web"],
      "enabled" => false
    }
  ]

  items.each {|item| BaseItem.create(item)}
end

def seed_words
  BaseWord.collection.drop

  base_words = JSON.parse(File.read("output_base_words.json"))
  base_words.each {|base_word| BaseWord.create(base_word)}
end

def seed_skills
  BaseSkill.collection.drop

  base_skills = JSON.parse(File.read("output_base_skills.json"))
  base_skills.each {|skill| BaseSkill.create(skill)}

  skills_tree = [
    ["null", "en-vi_co_ban_1", "null"],
    ["en-vi_co_ban_2", "en-vi_nhung_nhom_tu_thong_dung"],
    ["en-vi_mon_an", "en-vi_dong_vat", "null"],
    ["null", "en-vi_so_nhieu"],
    ["null", "en-vi_dai_tu_so_huu", "en-vi_dai_tu_khach_quan"],

    ["null", "en-vi_quan_ao"],
    ["en-vi_dong_tu_hien_tai_1", "en-vi_mau_sac", "null"],
    ["en-vi_cau_hoi", "en-vi_lien_tu"],
    ["null", "en-vi_gioi_tu", "null"],
    ["en-vi_ngay_va_gio", "en-vi_gia_dinh"],

    ["null", "en-vi_nghe_nghiep", "null"],
    ["en-vi_tinh_tu_1", "en-vi_dong_tu_hien_tai_2"],
    ["null", "en-vi_pho_tu", "null"],
    ["en-vi_noi_chon", "en-vi_tan_ngu"],
    ["null", "en-vi_con_nguoi", "null"],
    ["en-vi_du_lich", "en-vi_tu_han_dinh"],
    ["null", "en-vi_so", "null"],
    ["en-vi_dong_tu_thi_hien_tai_3", "en-vi_giao_duc"],

    ["null", "en-vi_dong_tu_thi_qua_khu", "en-vi_dong_tu_nguyen_mau"],
    ["en-vi_dong_tu_qua_khu_2", "null"],
    ["en-vi_tu_truu_tuong_1", "en-vi_bien_to_tinh_tu", "null"],
    ["en-vi_dong_tu_thi_hien_tai_hoan_thanh", "null"],
    ["null", "en-vi_dong_tu_nguyen_mau_2", "en-vi_dai_tu_quan_he"],
    ["null", "en-vi_dong_tu_thi_qua_khu_hoan_thanh"],
    ["en-vi_tu_vung_truu_tuong_2", "en-vi_dai_tu_phan_than"],
    ["en-vi_thien_nhien", "null"],

    ["en-vi_danh_dong_tu_gerunds", "en-vi_the_thao", "null"],
    ["en-vi_nghe_thuat", "en-vi_truyen_thong"],
    ["en-vi_y_te", "en-vi_dong_tu_thi_tuong_lai", "en-vi_chinh_tri"],
    ["en-vi_dong_tu_thi_tuong_lai_don", "en-vi_khoa_hoc"],
    ["null", "en-vi_dong_tu_tuong_lai_hoan_thanh", "null"],
    ["en-vi_kinh_doanh", "en-vi_dong_tu_khiem_khuyet_modal_auxiliaries"],
    ["en-vi_su_kien", "en-vi_tu_vung_dieu_kien_hoan_thanh", "en-vi_dac_tinh"]
  ]

  skills_tree.each_with_index {|skills, row|
    skills.each_with_index {|skill_id, column|
      BaseSkill.find(skill_id).set(coordinate: [row, column]) if skill_id != "null"
    }
  }

  [
    {"name"=>"Cơ bản 1", "title"=>"Cơ bản 1", "slug"=>"Cơ bản 1", "theme_color"=>"#33b5e5"},
    {"name"=>"Cơ bản 2", "title"=>"Cơ bản 2", "slug"=>"Cơ bản 2", "theme_color"=>"#33b5e5"},
    {"name"=>"COMMON-PHRASES", "title"=>"Những nhóm từ thông dụng", "slug"=>"Cụm từ", "theme_color"=>"#99cc00"},
    {"name"=>"FOOD", "title"=>"Món ăn", "slug"=>"Món ăn", "theme_color"=>"#ff4444"},
    {"name"=>"ANIMALS", "title"=>"Động vật", "slug"=>"Động vật", "theme_color"=>"#33b5e5"},
    {"name"=>"Số nhiều", "title"=>"Số nhiều", "slug"=>"Số nhiều", "theme_color"=>"#99cc00"},
    {"name"=>"POSSESSIVES", "title"=>"Đại từ sở hữu", "slug"=>"Sở hữu", "theme_color"=>"#33b5e5"},
    {"name"=>"PRONOUNS-OBJECTIVE", "title"=>"Đại từ khách quan", "slug"=>"Đ.t K.quan", "theme_color"=>"#ff4444"},
    {"name"=>"Quần áo", "title"=>"Quần áo", "slug"=>"Quần áo", "theme_color"=>"#99cc00"},
    {"name"=>"Động từ hiện tại 1", "title"=>"Động từ hiện tại 1", "slug"=>"Động từ", "theme_color"=>"#ff4444"},
    {"name"=>"COLORS", "title"=>"Màu sắc", "slug"=>"Màu sắc", "theme_color"=>"#99cc00"},
    {"name"=>"QUESTIONS", "title"=>"Câu hỏi", "slug"=>"Câu hỏi", "theme_color"=>"#ff4444"},
    {"name"=>"CONJUNCTIONS", "title"=>"Liên từ", "slug"=>"Liên từ", "theme_color"=>"#33b5e5"},
    {"name"=>"Giới từ", "title"=>"Giới từ", "slug"=>"Giới từ", "theme_color"=>"#99cc00"},
    {"name"=>"DATES-AND-TIME", "title"=>"Ngày và Giờ", "slug"=>"Thời gian", "theme_color"=>"#33b5e5"},
    {"name"=>"FAMILY", "title"=>"Gia đình", "slug"=>"Gia đình", "theme_color"=>"#99cc00"},
    {"name"=>"OCCUPATIONS", "title"=>"Nghề nghiệp", "slug"=>"Nghề nghiệp", "theme_color"=>"#ff4444"},
    {"name"=>"ADJECTIVES", "title"=>"Tính từ 1", "slug"=>"Tính từ 1", "theme_color"=>"#33b5e5"},
    {"name"=>"VERBS-PRESENT2", "title"=>"Động từ: Hiện tại 2", "slug"=>"Đ.từ H.t 2", "theme_color"=>"#99cc00"},
    {"name"=>"ADVERBS", "title"=>"Phó từ", "slug"=>"Phó từ", "theme_color"=>"#33b5e5"},
    {"name"=>"PLACES", "title"=>"Nơi chốn", "slug"=>"Nơi chốn", "theme_color"=>"#33b5e5"},
    {"name"=>"OBJECTS", "title"=>"Tân ngữ", "slug"=>"Tân ngữ", "theme_color"=>"#ff4444"},
    {"name"=>"Con người", "title"=>"Con người", "slug"=>"Con người", "theme_color"=>"#99cc00"},
    {"name"=>"Du lịch", "title"=>"Du lịch", "slug"=>"Du lịch", "theme_color"=>"#33b5e5"},
    {"name"=>"Từ hạn định", "title"=>"Từ hạn định", "slug"=>"Từ h.định", "theme_color"=>"#ff4444"},
    {"name"=>"Số", "title"=>"Số", "slug"=>"Số", "theme_color"=>"#33b5e5"},
    {"name"=>"Động từ: Thì hiện tại 3", "title"=>"Động từ: Thì hiện tại 3", "slug"=>"Đ.từ H.t 3", "theme_color"=>"#33b5e5"},
    {"name"=>"Giáo dục", "title"=>"Giáo dục", "slug"=>"Giáo dục", "theme_color"=>"#ff4444"},
    {"name"=>"Động từ: Thì quá khứ", "title"=>"Động từ: Thì quá khứ", "slug"=>"Đ.từ Q.khứ", "theme_color"=>"#ff4444"},
    {"name"=>"VERBS-INFINITIVE", "title"=>"Động từ: Nguyên mẫu", "slug"=>"Đ.từ Ng.mẫu", "theme_color"=>"#99cc00"},
    {"name"=>"VERBS-PAST2", "title"=>"Động từ: Quá khứ 2", "slug"=>"Đ.từ Q.k 2", "theme_color"=>"#33b5e5"},
    {"name"=>"ABSTRACT-OBJECTS", "title"=>"Từ trừu tượng 1", "slug"=>"Tr.tượng 1", "theme_color"=>"#ff4444"},
    {"name"=>"Biến tố tính từ", "title"=>"Biến tố tính từ", "slug"=>"B.tố T.từ", "theme_color"=>"#99cc00"},
    {"name"=>"Động từ: Thì hiện tại hoàn thành", "title"=>"Động từ: Thì hiện tại hoàn thành", "slug"=>"H.tại H.thành", "theme_color"=>"#ff4444"},
    {"name"=>"VERBS-INFINITIVE2", "title"=>"Động từ: Nguyên mẫu 2", "slug"=>"Đ.từ Ng.mẫu 2", "theme_color"=>"#99cc00"},
    {"name"=>"Đại từ quan hệ", "title"=>"Đại từ quan hệ", "slug"=>"Đại từ Q.hệ", "theme_color"=>"#99cc00"},
    {"name"=>"Động từ: Thì quá khứ hoàn thành", "title"=>"Động từ: Thì quá khứ hoàn thành", "slug"=>"Q.khứ H.thành", "theme_color"=>"#33b5e5"},
    {"name"=>"ABSTRACT-OBJECTS2", "title"=>"Từ vựng trừu tượng 2", "slug"=>"Tr.tượng 2", "theme_color"=>"#ff4444"},
    {"name"=>"PRONOUNS-REFLEXIVE", "title"=>"Đại từ phản thân", "slug"=>"Đ.từ P.thân", "theme_color"=>"#33b5e5"},
    {"name"=>"Thiên nhiên", "title"=>"Thiên nhiên", "slug"=>"Thiên nhiên", "theme_color"=>"#ff4444"},
    {"name"=>"Danh động từ (Gerunds)", "title"=>"Danh động từ (Gerunds)", "slug"=>"Danh Đ.từ", "theme_color"=>"#99cc00"},
    {"name"=>"Thể thao", "title"=>"Thể thao", "slug"=>"Thể thao", "theme_color"=>"#99cc00"},
    {"name"=>"Nghệ thuật", "title"=>"Nghệ thuật", "slug"=>"Nghệ thuật", "theme_color"=>"#ff4444"},
    {"name"=>"Truyền thông", "title"=>"Truyền thông", "slug"=>"Truyền thông", "theme_color"=>"#33b5e5"},
    {"name"=>"Y tế", "title"=>"Y tế", "slug"=>"Y tế", "theme_color"=>"#ff4444"},
    {"name"=>"Động từ: Thì tương lai", "title"=>"Động từ: Thì tương lai", "slug"=>"Đ.từ T.lai", "theme_color"=>"#99cc00"},
    {"name"=>"Chính trị", "title"=>"Chính trị", "slug"=>"Chính trị", "theme_color"=>"#99cc00"},
    {"name"=>"VERBS-FUTURE-PHRASAL", "title"=>"Động từ: Thì tương lai đơn", "slug"=>"Đ.từ T.lai", "theme_color"=>"#33b5e5"},
    {"name"=>"Khoa học", "title"=>"Khoa học", "slug"=>"Khoa học", "theme_color"=>"#33b5e5"},
    {"name"=>"Động từ: Tương lai hoàn thành", "title"=>"Động từ: Tương lai hoàn thành", "slug"=>"T.lai H.thành", "theme_color"=>"#ff4444"},
    {"name"=>"BUSINESS", "title"=>"Kinh doanh", "slug"=>"Kinh doanh", "theme_color"=>"#33b5e5"},
    {"name"=>"Động từ khiếm khuyết (Modal Auxiliaries)", "title"=>"Động từ khiếm khuyết (Modal Auxiliaries)", "slug"=>"Khiếm khuyết", "theme_color"=>"#99cc00"},
    {"name"=>"EVENTS", "title"=>"Sự kiện", "slug"=>"Sự kiện", "theme_color"=>"#ff4444"},
    {"name"=>"Từ vựng: Điều kiện hoàn thành", "title"=>"Từ vựng: Điều kiện hoàn thành", "slug"=>"Đ.kiện H.thành", "theme_color"=>"#33b5e5"},
    {"name"=>"Đặc tính", "title"=>"Đặc tính", "slug"=>"Đặc tính", "theme_color"=>"#ff4444"}
  ].each_with_index {|data, order|
    base_skill = BaseSkill.find_by(name: data['name'])
    base_skill.update_attributes(data)
    base_skill.set(order: order)
  }

  {
    "en-vi_co_ban_1" => ["en-vi_co_ban_2", "en-vi_nhung_nhom_tu_thong_dung"],
    "en-vi_nhung_nhom_tu_thong_dung" => ["en-vi_mon_an", "en-vi_dong_vat"],
    "en-vi_dong_vat" => ["en-vi_so_nhieu"],
    "en-vi_so_nhieu" => ["en-vi_dai_tu_so_huu", "en-vi_dai_tu_khach_quan"],
    "en-vi_dai_tu_khach_quan" => ["en-vi_quan_ao"],
    "en-vi_quan_ao" => ["en-vi_dong_tu_hien_tai_1", "en-vi_mau_sac"],
    "en-vi_mau_sac" => ["en-vi_cau_hoi", "en-vi_lien_tu"],
    "en-vi_lien_tu" => ["en-vi_gioi_tu"],
    "en-vi_gioi_tu" => ["en-vi_ngay_va_gio", "en-vi_gia_dinh"],
    "en-vi_gia_dinh" => ["en-vi_nghe_nghiep"],
    "en-vi_nghe_nghiep" => ["en-vi_tinh_tu_1", "en-vi_dong_tu_hien_tai_2"],
    "en-vi_dong_tu_hien_tai_2" => ["en-vi_pho_tu"],
    "en-vi_pho_tu" => ["en-vi_noi_chon", "en-vi_tan_ngu"],
    "en-vi_tan_ngu" => ["en-vi_con_nguoi"],
    "en-vi_con_nguoi" => ["en-vi_du_lich", "en-vi_tu_han_dinh", "en-vi_so"],
    "en-vi_so" => ["en-vi_dong_tu_thi_hien_tai_3", "en-vi_giao_duc"],
    "en-vi_giao_duc" => ["en-vi_dong_tu_thi_qua_khu", "en-vi_dong_tu_nguyen_mau"],
    "en-vi_dong_tu_nguyen_mau" => ["en-vi_dong_tu_qua_khu_2"],
    "en-vi_dong_tu_qua_khu_2" => ["en-vi_tu_truu_tuong_1", "en-vi_bien_to_tinh_tu"],
    "en-vi_bien_to_tinh_tu" => ["en-vi_dong_tu_thi_hien_tai_hoan_thanh"],
    "en-vi_dong_tu_thi_hien_tai_hoan_thanh" => ["en-vi_dong_tu_nguyen_mau_2", "en-vi_dai_tu_quan_he"],
    "en-vi_dai_tu_quan_he" => ["en-vi_dong_tu_thi_qua_khu_hoan_thanh"],
    "en-vi_dong_tu_thi_qua_khu_hoan_thanh" => ["en-vi_tu_vung_truu_tuong_2", "en-vi_dai_tu_phan_than", "en-vi_thien_nhien"],
    "en-vi_thien_nhien" => ["en-vi_danh_dong_tu_gerunds", "en-vi_the_thao"],
    "en-vi_the_thao" => ["en-vi_nghe_thuat", "en-vi_truyen_thong"],
    "en-vi_truyen_thong" => ["en-vi_y_te", "en-vi_dong_tu_thi_tuong_lai", "en-vi_chinh_tri"],
    "en-vi_chinh_tri" => ["en-vi_dong_tu_thi_tuong_lai_don", "en-vi_khoa_hoc"],
    "en-vi_khoa_hoc" => ["en-vi_dong_tu_tuong_lai_hoan_thanh"],
    "en-vi_dong_tu_tuong_lai_hoan_thanh" => ["en-vi_kinh_doanh", "en-vi_dong_tu_khiem_khuyet_modal_auxiliaries"],
    "en-vi_dong_tu_khiem_khuyet_modal_auxiliaries" => ["en-vi_su_kien", "en-vi_tu_vung_dieu_kien_hoan_thanh", "en-vi_dac_tinh"]
  }.each { |skill_id, unlocked_skill_ids|
    BaseSkill.find(skill_id).set(unlocked_skill_ids: unlocked_skill_ids)
  }
end

def seeds_question_types_prob
  skills_probs = {}

  Dir['questions_type_probabilities/*'].each {|skill_path|
    skill_id = skill_path.split("/").last
    skills_probs[skill_id] = {}
    skills_probs[skill_id]["lessons"] ||= []
    skills_probs[skill_id]["probs"] ||= {}

    Dir["#{skill_path}/*.csv"].each_with_index {|lesson_path, index|
      csv = CSV.read(lesson_path)
      types = csv.shift

      probs = {}
      csv.each {|record|
        record.each_with_index {|column, index|
          next if index == 0

          probs[types[index]] ||= []
          probs[types[index]] << column.to_f
        }
      }

      probs.each {|k, v|
        probs[k] = v.sum / v.count
        skills_probs[skill_id]["probs"][k] ||= 0
        skills_probs[skill_id]["probs"][k] += probs[k]
      }

      skills_probs[skill_id]["lessons"] << probs
    }

    skills_probs[skill_id]["probs"].each {|k, v| skills_probs[skill_id]["probs"][k] /= skills_probs[skill_id]["lessons"].count}
  }

  skills_probs.each {|skill_id, probs|
    QuestionTypesProb.create({
      "base_skill_id" => skill_id,
      skill_probs: probs['probs'],
      lesson_probs: probs['lessons']
    })
  }
end

def link_skills_words_to_course
  BaseSkill.each {|bs|
    bs.lessons.each {|lesson|
      BaseWord.where(:id.in => lesson["base_word_ids"]).each {|bw|
        bw.base_lesson_ids ||= []
        bw.base_lesson_ids << bs._id.to_s + "|" + lesson["lesson_number"].to_s
        bw.base_lesson_ids.compact!
        bw.base_lesson_ids.uniq!
        bw.save
      }
    }

    BaseWord.where(:id.in => bs.base_word_ids).each {|bw|
      bw.base_skill_ids ||= []
      bw.base_skill_ids << bs._id.to_s
      bw.base_skill_ids.compact!
      bw.base_skill_ids.uniq!
      bw.save
    }
  }

  skills_tree = []
  BaseSkill.asc(:order).each {|bs|
    row, column = bs.coordinate
    skills_tree[row] ||= ["null"] * (row%2 == 0 ? 3 : 2);
    skills_tree[row][column] = bs.id
  }

  bc = BaseCourse.first
  checkpoint_positions = bc.checkpoints.map {|checkpoint|
    BaseSkill.find(checkpoint["highest_skill_id"]).coordinate.first
  }.sort.reverse

  checkpoint_positions.each_with_index {|pos, index| skills_tree.insert(pos+1, "null")}
  bc.base_skill_ids = BaseSkill.all.map(&:id)
  bc.skills_tree = skills_tree
  bc.save
end

def seed_sentences
  Sentence.collection.drop

  base_sentences = JSON.parse(File.read("output_sentences.json"))

  base_sentences.map {|sentence|
    p "Processing sentence #{sentence["compact_translation"]}"
    Sentence.create(sentence)
    true
  }.uniq

  Sentence.where(:text => nil).each {|sentence|
    p "Processing sentence #{sentence.id}: #{sentence.compact_translation}"

    sentence.token_groups = convert_compact_translation_to_tokens(sentence.compact_translation)
    sentence.text = expand(sentence.compact_translation).first
    sentence.save
  }
end

def link_sentences_to_sentences
  SentenceTranslation.collection.drop

  sentences_by_sentences = JSON.parse(File.read("output_sentences_link_sentences.json"))

  sentences_by_sentences.each {|couple|
    couple_sentences = couple.values

    p "Processing couple #{couple_sentences.join(" - ")}"
    related_sentences = Sentence.where(:compact_translation.in => couple_sentences)

    sentence_translation = SentenceTranslation.new
    sentence_translation.sentence_ids = related_sentences.map(&:id)
    sentence_translation.save

    related_sentences.each {|sentence|
      sentence.sentence_translation_ids ||= []
      sentence.sentence_translation_ids << sentence_translation.id
      sentence.sentence_translation_ids.uniq!
      sentence.save
    }
  }
end

def link_sentences_to_words
  sentences_by_base_words = JSON.parse(File.read("output_sentences_link_base_words.json"))

  sentences_by_base_words.each {|relationship|
    sentence = Sentence.where(compact_translation: relationship["compact_translation"]).first

    next unless sentence

    base_word_ids = relationship["base_word_ids"]
    p "Processing sentence #{sentence.text} with words: #{base_word_ids.join(", ")}"

    base_words = BaseWord.where(:id.in => base_word_ids)
    base_words.each {|bw|
      bw.sentence_ids ||= []
      bw.sentence_ids << sentence._id.to_s
      bw.save
    }

    sentence.base_word_ids ||= []
    sentence.base_word_ids += base_words.map(&:id)
    sentence.base_word_ids.compact!
    sentence.base_word_ids.uniq!
    sentence.save
  }

  BaseWord.where(:sentence_ids.nin => [nil, []]).each {|word|
    p "Processing word #{word.id}"

    word.sentence_ids.uniq!
    word.sentence_ids.compact!
    word.save

    Sentence.where(:id.in => word.sentence_ids).each {|sentence|
      p "   => link to sentence #{sentence.text}"
      sentence.base_word_ids ||= []
      sentence.base_word_ids << word.id
      sentence.save
    }
  }

  Sentence.where(:base_word_ids.nin => [nil, []]).each {|sentence|
    p "Processing sentence #{sentence.text}"
    sentence.base_word_ids.compact!
    sentence.base_word_ids.uniq!
    sentence.save
  }
end

def link_sentences_to_skills_lessons
  sentences_by_skills_lessons = JSON.parse(File.read("output_sentences_link_skills_lessons.json"))
  
  sentences_by_skills_lessons.each {|relation|
    sentence = Sentence.where(compact_translation: relation['compact_translation']).first

    next unless sentence

    p "Processing sentence #{sentence.text} with skills #{relation['base_skill_ids'].join(", ")} and lessons #{relation['base_lesson_ids'].join(", ")}"

    sentence.set(base_skill_ids: relation['base_skill_ids'])
    sentence.set(base_lesson_ids: relation['base_lesson_ids'])
  }
end

def seeds_crowdsource_sentences
  base_course = BaseCourse.first

  crowdsource_sentences = {}

  Sentence.where(lang: base_course.learning_lang, :sentence_translation_ids.nin => [nil, []]).each {|sentence|
    p "Processing sentence #{sentence.text}"

    sentence_ids = SentenceTranslation.where(:id.in => sentence.sentence_translation_ids).map(&:sentence_ids).flatten.uniq

    learning_sentence_ids = Sentence.where(lang: base_course.learning_lang, :id.in => sentence_ids).asc(:group).asc(:text).map(&:id)
    primary_sentence_ids = Sentence.where(lang: base_course.primary_lang, :id.in => sentence_ids).asc(:group).asc(:text).map(&:id)

    key = [learning_sentence_ids, primary_sentence_ids].map(&:first).map(&:to_s).join("-")
    crowdsource_sentences[key] ||= {}

    crowdsource_sentences[key]["learning_lang_sentence_group_1_id"] = learning_sentence_ids.first
    crowdsource_sentences[key]["learning_lang_sentence_group_2_ids"] ||= []
    crowdsource_sentences[key]["learning_lang_sentence_group_2_ids"] += learning_sentence_ids
    crowdsource_sentences[key]["learning_lang_sentence_group_2_ids"].compact!
    crowdsource_sentences[key]["learning_lang_sentence_group_2_ids"].uniq!

    crowdsource_sentences[key]["primary_lang_sentence_group_1_id"] = primary_sentence_ids.first
    crowdsource_sentences[key]["primary_lang_sentence_group_2_ids"] ||= []
    crowdsource_sentences[key]["primary_lang_sentence_group_2_ids"] += primary_sentence_ids
    crowdsource_sentences[key]["primary_lang_sentence_group_2_ids"].compact!
    crowdsource_sentences[key]["primary_lang_sentence_group_2_ids"].uniq!
  }

  CrowdsourceSentence.collection.drop

  crowdsource_sentences.values.map {|crowdsource_sentence|
    CrowdsourceSentence.create crowdsource_sentence
    true
  }.uniq

  sentence = Sentence.where(text: "Water").first
  sentence.base_lesson_ids.delete "en-vi_co_ban_1|1"
  sentence.save

  sentences = Sentence.where(text: /duolingo/i)
  sids = sentences.map &:id
  SentenceTranslation.where(:sentence_ids.in => sids).destroy_all
  CrowdsourceSentence.or(
    CrowdsourceSentence.where(:primary_lang_sentence_group_1_id.in => sids).selector,
    CrowdsourceSentence.where(:primary_lang_sentence_group_2_ids.in => sids).selector,
    CrowdsourceSentence.where(:learning_lang_sentence_group_1_id.in => sids).selector,
    CrowdsourceSentence.where(:learning_lang_sentence_group_2_ids.in => sids).selector
  ).destroy_all
  sentences.destroy_all
end

def update_words_definitions
  csv_file = 'word_definitions.csv'
  csv_data = CSV.parse(File.read(csv_file))
  csv_data.delete csv_data.first

  word_definitions = {}
  csv_data.each {|row|
    word_definitions[row[1].strip] = row.last.split("\n").map(&:strip)
  }

  BaseWord.each {|bw| bw.set(definitions: word_definitions[bw.text])}
end

def seeds_db
  seed_courses
  seed_levels
  seeds_items
  seed_words
  seed_skills
  seeds_question_types_prob
  link_skills_words_to_course

  seed_sentences
  link_sentences_to_sentences
  link_sentences_to_words
  link_sentences_to_skills_lessons

  update_words_definitions
  seeds_crowdsource_sentences
end