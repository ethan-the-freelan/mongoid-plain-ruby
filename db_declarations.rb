require 'mongoid'
require 'json'

# Mongoid.load!("mongoid.yml", :development)
Mongoid.load!("mongoid.yml", :production)

class BaseCourse
  include Mongoid::Document

  field :name, type: String
  field :primary_lang, type: String
  field :learning_lang, type: String
  field :url_img, type: String
  field :base_skill_ids, type: Array
  field :checkpoints, type: Array
  field :skills_tree, type: Array
end

class BaseSkill
  include Mongoid::Document

  field :base_course_id, type: String
  field :name, type: String
  field :title, type: String
  field :slug, type: String
  field :order, type: Integer
  field :coordinate, type: Array
  field :lessons, type: Array
  field :base_word_ids, type: Array
  field :theme_color, type: String
  field :unlocked_skill_ids, type: Array
end

class QuestionTypesProb
  include Mongoid::Document

  field :base_skill_id
  field :skill_probs
  field :lesson_probs
end

class OwnedSkill
  include Mongoid::Document

  field :user_id
  field :skill_id
  field :unlocked
  field :finished_lesson
  field :weakest_word_ids
  field :base_course_id
  field :strength
  field :order
end

class BaseWord
  include Mongoid::Document

  field :base_skill_ids, type: Array
  field :base_lesson_ids, type: Array

  field :text
  field :is_objective, type: Boolean, default: true
  field :lang, type: String
  field :image_files, type: Array
  field :sentence_ids, type: Array
  field :sound, type: String
  field :definitions, type: Array
end

class Sentence
  include Mongoid::Document

  field :sentence_translation_ids, type: Array
  field :related_sentence_ids, type: Array
  field :base_skill_ids, type: Array
  field :base_lesson_ids, type: Array

  field :compact_translation, type: String
  field :text, type: String
  field :token_groups, type: Array
  field :pattern_score, type: Float, default: 0
  field :group, type: Integer
  field :words, type: Array
  field :lang, type: String
  field :normal_audio_file, type: String
  field :slow_audio_file, type: String
  field :answers, type: Integer, default: 0
  field :feedbacks, type: Integer, default: 0
  field :base_word_ids, type: Array
end

class SentenceTranslation
  include Mongoid::Document
  field :sentence_ids, type: Array
end

class CrowdsourceSentence
  include Mongoid::Document

  field :primary_lang_sentence_group_1_id, type: BSON::ObjectId
  field :primary_lang_sentence_group_2_ids, type: Array
  field :primary_lang_sentence_group_3_ids, type: Array
  field :primary_lang_sentence_group_4_ids, type: Array
  field :primary_lang_sentence_group_5_ids, type: Array

  field :learning_lang_sentence_group_1_id, type: BSON::ObjectId
  field :learning_lang_sentence_group_2_ids, type: Array
  field :learning_lang_sentence_group_3_ids, type: Array
  field :learning_lang_sentence_group_4_ids, type: Array
  field :learning_lang_sentence_group_5_ids, type: Array  
end

class BaseItem
  include Mongoid::Document

  field :section, type: String
  field :localized, type: Hash
  field :price, type: Integer
  field :consumable, type: Boolean
  field :max_owned_amount, type: Integer
  field :platforms, type: Array
  field :enabled, type: Boolean
end

class BaseLevel
  include Mongoid::Document

  field :xp, type: Integer
  field :exp, type: Integer
  field :title, type: String
end

class User
  include Mongoid::Document

  field :name
  field :username
  field :password
  field :fb_Id
  field :gmail
  field :url_avatar
  field :email
  field :ip
  field :status
  field :role
  field :auth_token
  field :created
  field :exp
  field :earned_exp_week
  field :earned_exp_month
  field :owned_word_ids
  field :owned_skill_ids
  field :owned_course_ids
  field :current_course
  field :level
  field :level_title
  field :owned_achievement_ids
  field :virtual_money
  field :current_skill_id
  field :is_trial
  field :is_beginner
  field :checkpoints
  field :exam_token
  field :current_combo_days
  field :last_combo_earned_day
  field :chart
  field :follower_ids
  field :following_ids
  field :audio_enable
  field :listen_question_enable
  field :settings
end