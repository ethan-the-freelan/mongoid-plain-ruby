def convert_compact_translation_to_tokens(text)
  scan_pattern = /\[([^\]]*)\]/
  split_pattern = /\[[^\]]*\]/

  splited_tokens = text.split(split_pattern)

  matched_tokens = text.scan(scan_pattern).map {|m|
    ms = m.first
    arr = ms.split("/")
    arr << "" if ms.end_with?("/")
    arr
  }

  if matched_tokens.count < splited_tokens.count
    matched_tokens += [""] * (splited_tokens.count - matched_tokens.count)
  elsif matched_tokens.count > splited_tokens.count
    splited_tokens += [""] * (matched_tokens.count - splited_tokens.count)
  end

  all_tokens = [splited_tokens, matched_tokens].transpose.inject(&:+) - [[""]] - [""]

  all_tokens
end

def expand_compact_translation(tokens_group)
  tokens = tokens_group.first
  tokens = [tokens] unless tokens.is_a?(Array)

  return tokens if tokens_group.count == 1

  tokens.map {|token|
    expand_compact_translation(tokens_group[1..-1]).map {|next_token|
      token + next_token
    }
  }.flatten
end

def expand(text)
  all_tokens = convert_compact_translation_to_tokens(text)
  
  results = expand_compact_translation(all_tokens).map(&:strip).map {|ss|
    result = ss.gsub(/\s+(\s|\.)/, '\1')
    components = result.split(" ")
    components.first.capitalize!
    result = components.join(" ")
  }

  results
end